<?php get_header(); ?>


	<div id="content">
		<div class="sliderArea">
			<div class="sliderWide">
				<ul class="slick-slider">
				<?php
					$args = array(
						'post_type' => 'post', // 投稿タイプ
						'posts_per_page' => 8, // 表示件数の指定
						//'category_name' => 'mancolumn', // カテゴリをスラッグで指定
						'orderby' => 'date', // 表示順の基準
    					'order' => 'DESC' // 昇順・降順
					);
					$posts = get_posts( $args );
					
					foreach ( $posts as $post ): // ループの開始
					setup_postdata( $post ); // 記事データの取得
				?>
				<li class="slick-item"><a href="#"><?php the_post_thumbnail('full'); ?></a></li>
				<?php
					endforeach; // ループの終了
					wp_reset_postdata(); // 直前のクエリを復元する
				?>
			</div>
		</div>

		<!-- 新着記事 -->
		<section class="new_post">
			<h2>新着記事</h2>
			<div id="post_pr">
				<?php
					$args = array(
						'post_type' => 'post', // 投稿タイプ
						'posts_per_page' => 8, // 表示件数の指定
						//'category_name' => 'mancolumn', // カテゴリをスラッグで指定(カテゴリ別で分ける場合)
						'orderby' => 'date', // 表示順の基準
    					'order' => 'DESC' // 昇順・降順
					);
					$posts = get_posts( $args );
					
					foreach ( $posts as $post ): // ループの開始
					setup_postdata( $post ); // 記事データの取得
				?>
				<div class="post_ct">
					<div class="ct_img">
						<!--<div class="category"><?php //the_category(' / '); ?></div>-->
						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full'); ?></a>
					</div>
					<div class="ct_detail">
						<div class="date"><?php the_time( get_option( 'date_format' ) ); ?></div>
						<div class="title">
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</div>
					</div>
				</div>
				<?php
					endforeach; // ループの終了
					wp_reset_postdata(); // 直前のクエリを復元する
				?>
			</div>
		</section>

		<!-- マンコラム -->
		<section class="mancolumn">
			<h2>マンコラム</h2>
			<div id="post_pr">
				<?php
					$args = array(
						'post_type' => 'post', // 投稿タイプ
						'posts_per_page' => 8, // 表示件数の指定
						'category_name' => 'mancolumn', // カテゴリをスラッグで指定
						'orderby' => 'date', // 表示順の基準
    					'order' => 'DESC' // 昇順・降順
					);
					$posts = get_posts( $args );
					
					foreach ( $posts as $post ): // ループの開始
					setup_postdata( $post ); // 記事データの取得
				?>		
				<div class="post_ct">
					<div class="ct_img">
						<a href="<?php the_permalink(); ?>">
							<?php the_post_thumbnail('full'); ?>
						</a>
					</div>
					<div class="title">
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</div>
				</div>
				<?php
					endforeach; // ループの終了
					wp_reset_postdata(); // 直前のクエリを復元する
				?>
			</div>
		</section>	
					<!-- マンコラム -->
		<section class="picpicture">
			<h2>ピクピクチャー</h2>
			<div id="post_pr">
				<?php
					$args = array(
						'post_type' => 'post', // 投稿タイプ
						'posts_per_page' => 8, // 表示件数の指定
						'category_name' => 'mancolumn', // カテゴリをスラッグで指定
						'orderby' => 'date', // 表示順の基準
    					'order' => 'DESC' // 昇順・降順
					);
					$posts = get_posts( $args );
					
					foreach ( $posts as $post ): // ループの開始
					setup_postdata( $post ); // 記事データの取得
				?>		
				<div class="post_ct">
					<div class="ct_img">
						<a href="<?php the_permalink(); ?>">
							<?php the_post_thumbnail('full'); ?>
						</a>
					</div>
					<div class="title">
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</div>
				</div>
				<?php
					endforeach; // ループの終了
					wp_reset_postdata(); // 直前のクエリを復元する
				?>
			</div>
		</section>	
	</div>
	<style type="text/css">
	<!--
	/* スライダー */

	.slick-item img{
		width: 100%;
		height: 100%;
	}

	.slick-dotted.slick-slider{
		width: 1260px;
		margin: 50px auto 50px;
	}

	.slick-item{
		margin: 0 18px;
	}
	/*.sliderArea {
		background: #fff;
		overflow: hidden;
	}
.sliderWide {
    max-width: 100%;
    margin: 0 auto;
    padding: 30px 0;
}
.sliderWide .slick-arrow {
    opacity: 0;
    transition-duration: 0.3s;
}
.sliderWide:hover .slick-arrow {
    opacity: 1;
}
.slider {
    max-width: 280px;
    margin: 0 auto;
    padding: 0;
}
.slider .slick-list {
    overflow: visible;
}
.slider .slick-track {}
.slider .slick-slide {
    padding: 0 10px;
    opacity: .5;
    transition-duration: 0.3s;
}
.slider .slick-slide.slick-current {
    opacity: 1;
}
.slider .slick-slide a {
    display: block;
}
.slider .slick-slide a:hover {
    display: block;
    opacity: .7;
}
.slider img {
    height: auto;
    width: 100%;
}
.slick-prev, .slick-next {
    z-index: 1;
}
.slick-dots {
    bottom: -33px;
}*/

	/* CSSは別ファイルにまとめます　実験段階なのでベタですまんこ */
	#content section{
		padding-top: 30px;
		padding-bottom: 30px;
	}

	a:link, a:visited, a:hover, a:active {
		color: black;
		text-decoration: none;
	}
	#post_pr{
		display: grid;
		width: 1100px;
		margin: auto;
		grid-template-columns: 1fr 1fr 1fr 1fr;
		grid-gap: 20px;
	}

	.post_ct{
		width: 250px;
	}

	.post_ct .title{
		margin: 5px;
	}

	.post_ct .title a{
		font-weight: 800;
	}

	.post_ct .ct_img{
		height: 170px;
		position: relative;
	}


	.post_ct .ct_img img{
		width: 100%;
    	height: 100%;
	}

	.post_ct .date{
		font-size: 14px;
		margin: 5px;
	}

	.post_ct .category{
		background: pink;
		position: absolute;
		top: 10px;
		left: 10px;
		font-size: 12px;
		width: 100px;
		text-align: center;
		/* height: 18px; */
		vertical-align: middle;
	}

	#content h2{
		text-align: center;
		margin-bottom: 30px;
	}

	/* 新着記事セクション */
	.new_post{
		background: white;
		/*padding-top: 20px;*/
	}

	/* マンコラムセクション */
	.mancolumn{
		background: pink;
	}

	/* スマホ〜〜〜 */
	@media (max-width: 812px){
		#post_pr{
			display: block;
			width: 100vw;
		}

		.post_ct{
			width: calc( 100vw - 50px);
			display: flex;
			padding-bottom: 30px;
			margin: auto;
		}

		.post_ct .ct_img {
			height: 100px;
			display: inline-block;
			width: 50%;
		}

		.post_ct .title{
			display: inline-block;
		}

		.post_ct .ct_detail{
			margin-left: 15px;
		}

		.post_ct .date {
			font-size: 13px;
			margin: 0px;

		}

		


	

	-->
	</style>


<?php get_footer(); ?>
